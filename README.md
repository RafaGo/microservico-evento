README - API de Eventos
Esta é uma API de eventos que permite o cadastro, consulta, atualização e exclusão de eventos.

A API é construída utilizando a arquitetura RESTful e é implementada em Java com o framework Springboot. O banco de dados utilizado é o MongoDb, Os testes foram feitos com JUnit.

Endpoints
A API possui os seguintes endpoints:

GET /eventos
Retorna uma lista de todos os eventos cadastrados no sistema.

GET /eventos/{id}
Retorna as informações de um evento específico.

POST /eventos
Cadastra um novo evento.

PUT /eventos/{id}
Atualiza as informações de um evento existente.

DELETE /eventos/{id}
Exclui um evento existente.
