package com.event.msevent.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.event.msevent.dto.EventDTO;
import com.event.msevent.exception.NotFoundException;
import com.event.msevent.service.EventService;

@RestController
@RequestMapping("/events")
public class EventController {

    @Autowired
    EventService service;

    @GetMapping
    public ResponseEntity<List<EventDTO>> listarTodos() {
        List<EventDTO> lista = this.service.listarTodos();
        return ResponseEntity.ok(lista);
    }

    @GetMapping("/{id}")
    public ResponseEntity<EventDTO> buscar(@PathVariable String id) throws NotFoundException {
        EventDTO busca = this.service.buscar(id);
        return ResponseEntity.ok(busca);
    }

    @PostMapping
    public ResponseEntity<EventDTO> inserir(@RequestBody EventDTO dto) throws URISyntaxException {

        EventDTO salvo = this.service.inserir(dto);
        return ResponseEntity.created(new URI("/events")).body(salvo);
    }

    @PutMapping("/{id}")
    public ResponseEntity<EventDTO> atualizar(@PathVariable String id, @RequestBody EventDTO dto)
            throws NotFoundException {

        EventDTO atualiza = this.service.atualizar(id, dto);
        return ResponseEntity.ok(atualiza);

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletar(@PathVariable String id) {
        this.service.delete(id);
        return ResponseEntity.noContent().build();
        // ola
    }
}
