package com.event.msevent.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EventDTO {

    private String id;
    private String nome;
    private String descricao;
    private String endereco;
    private String data;

}
