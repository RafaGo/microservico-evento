package com.event.msevent.exception;

import java.io.Serializable;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class EventException implements Serializable {

    private int code;
    private String message;
    private String timestamp;
}
