package com.event.msevent.mapper;

import com.event.msevent.dto.EventDTO;
import com.event.msevent.model.Event;

public class EventMapper {

    public static Event dtoToModel(EventDTO dto) {
        Event novo = new Event();
        novo.setId(dto.getId());
        novo.setNome(dto.getNome());
        novo.setEndereco(dto.getEndereco());
        novo.setDescricao(dto.getDescricao());
        novo.setData(dto.getData());
        return novo;

    }

    public static EventDTO modelToDto(Event model) {
        EventDTO novo = new EventDTO();
        novo.setId(model.getId());
        novo.setNome(model.getNome());
        novo.setEndereco(model.getEndereco());
        novo.setDescricao(model.getDescricao());
        novo.setData(model.getData());
        return novo;

    }
}
