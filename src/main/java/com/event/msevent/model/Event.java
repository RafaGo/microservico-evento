package com.event.msevent.model;

import org.springframework.data.annotation.Id;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Event {
    @Id
    private String id;
    private String nome;
    private String descricao;
    private String endereco;
    private String data;

}
