package com.event.msevent.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.event.msevent.model.Event;

public interface EventRepository extends MongoRepository<Event, String> {

}
