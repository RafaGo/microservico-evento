package com.event.msevent.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.event.msevent.dto.EventDTO;
import com.event.msevent.exception.NotFoundException;
import com.event.msevent.mapper.EventMapper;
import com.event.msevent.model.Event;
import com.event.msevent.repository.EventRepository;

@Service
public class EventService {

    @Autowired
    EventRepository repository;

    public EventDTO inserir(EventDTO dto) {
        Event event = EventMapper.dtoToModel(dto);
        Event salvo = this.repository.insert(event);
        EventDTO novo = EventMapper.modelToDto(salvo);
        return novo;

    }

    public List<EventDTO> listarTodos() {
        List<Event> lista = this.repository.findAll();
        List<EventDTO> listaDto = new ArrayList<EventDTO>();
        for (var item : lista) {
            EventDTO novo = EventMapper.modelToDto(item);
            listaDto.add(novo);
        }
        return listaDto;

    }

    public EventDTO atualizar(String id, EventDTO dto) throws NotFoundException {
        this.repository.findById(id)
                .orElseThrow(() -> new NotFoundException());
        Event novo = EventMapper.dtoToModel(dto);
        novo.setId(id);
        EventDTO atualizarDto = EventMapper.modelToDto(this.repository.save(novo));
        return atualizarDto;
    }

    public EventDTO buscar(String id) throws NotFoundException {
        Event funcionario = this.repository.findById(id)
                .orElseThrow(() -> new NotFoundException());
        return EventMapper.modelToDto(funcionario);

    }

    public void delete(String id) {
        this.repository.deleteById(id);
    }

}
