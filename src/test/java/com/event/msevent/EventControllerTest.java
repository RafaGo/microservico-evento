package com.event.msevent;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.AutoConfigureDataMongo;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import com.event.msevent.dto.EventDTO;
import com.event.msevent.model.Event;
import com.event.msevent.repository.EventRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

@AutoConfigureMockMvc
@AutoConfigureDataMongo
@SpringBootTest
@ActiveProfiles("test")
public class EventControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private EventRepository eventRepository;

    @BeforeEach
    public void setUp() {
        Event event = new Event();
        event.setId("12345");

        this.eventRepository.save(event);
    }

    @Test
    public void eventTestGetAll() throws Exception {
        this.mockMvc.perform(get("/events"))
                .andExpect(status().isOk());

    }

    @Test
    public void eventTestUpdate() throws Exception {
        EventDTO dto = new EventDTO();
        String strDto = new ObjectMapper().writeValueAsString(dto);
        this.mockMvc.perform(
                put("/events/12345").contentType("application/json").content(strDto))
                .andExpect(status().isOk());

    }

    @Test
    public void eventTestPost() throws Exception {
        EventDTO dto = new EventDTO();
        String strDto = new ObjectMapper().writeValueAsString(dto);
        this.mockMvc.perform(post("/events").contentType("application/json").content(strDto))
                .andExpect(status().isCreated());
    }

    @Test
    public void eventTestGet() throws Exception {
        this.mockMvc.perform(get("/events/12345"))
                .andExpect(status().isOk());
    }

    @Test
    public void eventTestDelete() throws Exception {
        this.mockMvc.perform(delete("/events/12345"))
                .andExpect(status().isNoContent());
    }

    @Test
    public void eventTestGetNotFound() throws Exception {
        this.mockMvc.perform(get("/events/222"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void eventTestUpdateGenericError() throws Exception {
        EventDTO dto = new EventDTO();
        String strDto = new ObjectMapper().writeValueAsString(dto);
        this.mockMvc.perform(
                put("/events/12345").content(strDto))
                .andExpect(status().isInternalServerError());

    }
}